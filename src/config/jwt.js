import jwt from "jsonwebtoken";

const generateToken = (data) => {
  return jwt.sign({ data }, "hoanglam0869", { expiresIn: "1d" });
};

const checkToken = (token) => {
  return jwt.verify(token, "hoanglam0869");
};

const decodeToken = (token) => {
  return jwt.decode(token);
};

const tokenApi = (req, res, next) => {
  try {
    let { token } = req.headers;
    if (checkToken(token)) {
      next();
    }
  } catch (error) {
    res.status(401).send(error.message);
  }
};

export { generateToken, checkToken, decodeToken, tokenApi };
