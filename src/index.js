import express from "express";
import cors from "cors";
import rootRouter from "./routers/rootRouter.js";

const app = express();
app.listen(8081);
// chuyển sang định dạng json
app.use(express.json());
// CORS
app.use(cors({ origin: "*" }));
// định vị lại đường dẫn
app.use(express.static("."));
// đặt end point gốc
app.use("/api", rootRouter);
