import express from "express";
import {
  createImageReaction,
  deleteImage,
  deleteImageReaction,
  unsaveImage,
  getComments,
  getCreatedImagesByUser,
  getImageDetail,
  getImageReactionByUser,
  getImageReactions,
  getImages,
  getImagesByName,
  hasSavedImage,
  getSavedImagesByUser,
  saveComment,
  saveImage,
  updateImageReaction,
  uploadImage,
  updateImage,
  getThreeMostImageReactions,
  getImageReactionsAmount,
  updateComment,
  deleteComment,
} from "../controllers/imageController.js";
import upload from "../controllers/uploadController.js";
import { tokenApi } from "../config/jwt.js";

const imageRouter = express.Router();

imageRouter.post("/upload-image", tokenApi, upload.any(), uploadImage);
imageRouter.get("/get-image", getImages);
imageRouter.get("/get-image-by-name", getImagesByName);
imageRouter.get("/get-image-detail/:image_id", getImageDetail);
imageRouter.put("/update-image/:image_id", tokenApi, updateImage);
imageRouter.get("/get-comment/:image_id", getComments);
imageRouter.post("/save-comment", tokenApi, saveComment);
imageRouter.put("/update-comment/:comment_id", tokenApi, updateComment);
imageRouter.delete("/delete-comment/:comment_id", tokenApi, deleteComment);
imageRouter.get("/has-saved-image/:user_id/:image_id", tokenApi, hasSavedImage);
imageRouter.post("/save-image/:user_id/:image_id", tokenApi, saveImage);
imageRouter.delete("/unsave-image/:user_id/:image_id", tokenApi, unsaveImage);
imageRouter.get("/get-saved-image-by-user/:user_id", getSavedImagesByUser);
imageRouter.get("/get-created-image-by-user/:user_id", getCreatedImagesByUser);
imageRouter.delete("/delete-image/:image_id", tokenApi, deleteImage);
imageRouter.get("/get-image-reaction/:image_id", getImageReactions);
imageRouter.get(
  "/get-image-reaction-amount/:image_id",
  getImageReactionsAmount
);
imageRouter.get(
  "/get-three-most-image-reaction/:image_id",
  getThreeMostImageReactions
);
imageRouter.post("/create-image-reaction", tokenApi, createImageReaction);
imageRouter.get(
  "/get-image-reaction-by-user/:user_id/:image_id",
  tokenApi,
  getImageReactionByUser
);
imageRouter.put("/update-image-reaction", tokenApi, updateImageReaction);
imageRouter.delete(
  "/delete-image-reaction/:user_id/:image_id",
  tokenApi,
  deleteImageReaction
);

export default imageRouter;
