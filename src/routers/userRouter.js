import express from "express";
import {
  followUser,
  getAllUsers,
  getFollowers,
  getFollowersAmount,
  getFollowings,
  getFollowingsAmount,
  getUser,
  getUserById,
  hasFollowedUser,
  login,
  signUp,
  unfollowUser,
  updateUser,
  updateUserAvatar,
} from "../controllers/userController.js";
import { tokenApi } from "../config/jwt.js";
import upload from "../controllers/uploadController.js";

const userRouter = express.Router();

userRouter.get("/get-user", tokenApi, getUser);
userRouter.get("/get-all-user", getAllUsers);
userRouter.get("/get-user-by-id/:user_id", getUserById);
userRouter.put("/update-user/:user_id", tokenApi, updateUser);
userRouter.post("/signup", signUp);
userRouter.post("/login", login);
userRouter.post(
  "/update-user-avatar/:user_id",
  tokenApi,
  upload.single("file"),
  updateUserAvatar
);
userRouter.get("/get-follower/:following_id", getFollowers);
userRouter.get("/get-follower-amount/:following_id", getFollowersAmount);
userRouter.get("/get-following/:follower_id", getFollowings);
userRouter.get("/get-following-amount/:follower_id", getFollowingsAmount);
userRouter.post(
  "/follow-user/:follower_id/:following_id",
  tokenApi,
  followUser
);
userRouter.delete(
  "/unfollow-user/:follower_id/:following_id",
  tokenApi,
  unfollowUser
);
userRouter.get(
  "/has-followed-user/:follower_id/:following_id",
  tokenApi,
  hasFollowedUser
);

export default userRouter;
