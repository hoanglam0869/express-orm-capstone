import multer from "multer";
import path from "path";

const storage = multer.diskStorage({
  destination: process.cwd() + "/public/img",
  filename: (req, file, callback) => {
    let newName = new Date().getTime() + "_" + file.originalname;

    callback(null, newName);
  },
});

const storage1 = multer.memoryStorage();

const checkFileType = function (file, callback) {
  //Allowed file extensions
  const fileTypes = /jpeg|jpg|png|gif|svg/;

  //check extension names
  const extName = fileTypes.test(path.extname(file.originalname).toLowerCase());

  const mimeType = fileTypes.test(file.mimetype);

  if (mimeType && extName) {
    return callback(null, true);
  } else {
    callback("Error: You can Only Upload Images!!");
  }
};

const upload = multer({
  storage1,
  limits: {
    fileSize: 20971520,	// 20 MB
  },
  fileFilter: (req, file, callback) => {
    checkFileType(file, callback);
  },
});

export default upload;
