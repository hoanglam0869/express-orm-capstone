import { PrismaClient } from "@prisma/client";
import bcrypt from "bcrypt";
import { errorCode, failCode, successCode } from "../config/response.js";
import { decodeToken, generateToken } from "../config/jwt.js";
import fs from "fs";
import sharp from "sharp";
import { BASE_IMAGE_URL } from "../config/constant.js";

const prisma = new PrismaClient();

export const fixUser = (user) => {
  let { password, ...result } = user;
  if (result.avatar == "") {
    return result;
  } else {
    return {
      ...result,
      avatar: BASE_IMAGE_URL + result.avatar,
    };
  }
};

const signUp = async (req, res) => {
  try {
    let { email, password, full_name, age } = req.body;

    let checkUser = await prisma.user.findMany({
      where: {
        email,
      },
    });

    if (checkUser.length > 0) {
      failCode(res, "email", "Email đã tồn tại");
    } else {
      let user = {
        email,
        password: bcrypt.hashSync(password, 10),
        full_name,
        age: +age,
        avatar: "",
      };
      let newUser = await prisma.user.create({ data: user });
      let data = fixUser(newUser);

      let token = generateToken(data);
      successCode(res, token, "Đăng ký thành công");
    }
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const login = async (req, res) => {
  try {
    let { email, password } = req.body;
    let checkUser = await prisma.user.findFirst({
      where: {
        email,
      },
    });

    if (checkUser) {
      if (bcrypt.compareSync(password, checkUser.password)) {
        let user = fixUser(checkUser);

        let token = generateToken(user);
        successCode(res, token, "Đăng nhập thành công");
      } else {
        failCode(res, "password", "Mật khẩu không đúng!");
      }
    } else {
      failCode(res, "email", "Email không đúng!");
    }
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getUser = async (req, res) => {
  try {
    let { token } = req.headers;
    let decode = decodeToken(token);

    successCode(res, decode.data, "Lấy thông tin người dùng thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getAllUsers = async (req, res) => {
  try {
    let users = await prisma.user.findMany();
    let data = users.map((user) => fixUser(user));

    successCode(res, data, "Lấy thông tin tất cả người dùng thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getUserById = async (req, res) => {
  try {
    let { user_id } = req.params;

    let user = await prisma.user.findFirst({
      where: {
        user_id: +user_id,
      },
    });

    if (user) {
      let data = fixUser(user);
      successCode(res, data, "Lấy thông tin người dùng theo id thành công");
    } else {
      failCode(res, null, "Không tìm thấy người dùng");
    }
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const updateUser = async (req, res) => {
  try {
    let { user_id } = req.params;
    let { full_name, age } = req.body;

    let checkUser = await prisma.user.findFirst({
      where: {
        user_id: +user_id,
      },
    });

    if (checkUser) {
      let user = {
        full_name,
        age: +age,
      };

      let updateUser = await prisma.user.update({
        data: user,
        where: {
          user_id: +user_id,
        },
      });
      let data = fixUser(updateUser);

      let token = generateToken(data);
      successCode(res, token, "Cập nhật thông tin người dùng thành công");
    } else {
      failCode(res, null, "Không tìm thấy người dùng");
    }
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const updateUserAvatar = async (req, res) => {
  try {
    let { user_id } = req.params;

    let checkUser = await prisma.user.findFirst({
      where: {
        user_id: +user_id,
      },
    });

    if (checkUser) {
      let { buffer, originalname } = req.file;
      let timestamp = Date.now();
      let avatar = `${timestamp}-${originalname}.webp`;
      await sharp(buffer)
        .webp({ quality: 20 })
        .toFile(process.cwd() + "/public/img/" + avatar);

      let updateUser = { avatar };

      let user = await prisma.user.update({
        where: {
          user_id: +user_id,
        },
        data: updateUser,
      });

      if (checkUser.avatar != "") {
        let path = process.cwd() + "/public/img/" + checkUser.avatar;

        fs.unlink(path, (err) => {
          if (err) throw err;
        });
      }

      let data = fixUser(user);
      let token = generateToken(data);

      successCode(res, token, "Cập nhật hình đại diện thành công");
    } else {
      failCode(res, null, "Không tìm thấy người dùng");
    }
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getFollowers = async (req, res) => {
  try {
    let { following_id } = req.params;

    let followers = await prisma.follow.findMany({
      where: {
        following_id: +following_id,
      },
      include: {
        user_follow_follower_idTouser: true,
      },
      orderBy: { follow_id: "desc" },
    });
    let data = followers.map((follower) => {
      let { user_follow_follower_idTouser, ...result } = follower;
      return {
        ...result,
        user: fixUser(user_follow_follower_idTouser),
      };
    });
    successCode(res, data, "Lấy danh sách người theo dõi thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getFollowersAmount = async (req, res) => {
  try {
    let { following_id } = req.params;

    let data = await prisma.follow.count({
      where: {
        following_id: +following_id,
      },
    });

    successCode(res, data, "Lấy số lượng người theo dõi thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getFollowings = async (req, res) => {
  try {
    let { follower_id } = req.params;

    let followings = await prisma.follow.findMany({
      where: {
        follower_id: +follower_id,
      },
      include: {
        user_follow_following_idTouser: true,
      },
      orderBy: { follow_id: "desc" },
    });
    let data = followings.map((following) => {
      let { user_follow_following_idTouser, ...result } = following;
      return {
        ...result,
        user: fixUser(user_follow_following_idTouser),
      };
    });
    successCode(res, data, "Lấy danh sách người đang theo dõi thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getFollowingsAmount = async (req, res) => {
  try {
    let { follower_id } = req.params;

    let data = await prisma.follow.count({
      where: {
        follower_id: +follower_id,
      },
    });

    successCode(res, data, "Lấy số lượng người đang theo dõi thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const followUser = async (req, res) => {
  try {
    let { follower_id, following_id } = req.params;
    let follow = await prisma.follow.findFirst({
      where: {
        follower_id: +follower_id,
        following_id: +following_id,
      },
    });
    if (follow) {
      failCode(res, "", "Đã theo dõi người dùng này");
    } else {
      let newFollow = {
        follower_id: +follower_id,
        following_id: +following_id,
      };
      let data = await prisma.follow.create({ data: newFollow });
      successCode(res, data, "Theo dõi người dùng thành công");
    }
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const unfollowUser = async (req, res) => {
  try {
    let { follower_id, following_id } = req.params;
    let follow = await prisma.follow.findFirst({
      where: {
        follower_id: +follower_id,
        following_id: +following_id,
      },
    });
    if (follow) {
      await prisma.follow.delete({
        where: {
          follow_id: +follow.follow_id,
        },
      });
      successCode(res, "", "Hủy theo dõi người dùng thành công");
    } else {
      failCode(res, "", "Đã hủy theo dõi người dùng này");
    }
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const hasFollowedUser = async (req, res) => {
  try {
    let { follower_id, following_id } = req.params;
    let follow = await prisma.follow.findFirst({
      where: {
        follower_id: +follower_id,
        following_id: +following_id,
      },
    });
    if (follow) {
      successCode(res, true, "Đã theo dõi");
    } else {
      successCode(res, false, "Chưa theo dõi");
    }
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

export {
  getUser,
  getAllUsers,
  getUserById,
  updateUser,
  signUp,
  login,
  updateUserAvatar,
  getFollowers,
  getFollowersAmount,
  getFollowings,
  getFollowingsAmount,
  followUser,
  unfollowUser,
  hasFollowedUser,
};
