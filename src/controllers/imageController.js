import { PrismaClient } from "@prisma/client";
import { errorCode, failCode, successCode } from "../config/response.js";
import fs from "fs";
import sharp from "sharp";
import { fixUser } from "./userController.js";
import { BASE_IMAGE_URL } from "../config/constant.js";

const prisma = new PrismaClient();

export const fixImageURL = (images) => {
  if (images.length > 0) {
    let fixImages = images.map((image) => {
      image.image_url = BASE_IMAGE_URL + image.image_url;
      if (image.user) {
        image.user = fixUser(image.user);
      }
      return image;
    });
    return fixImages;
  } else {
    images.image_url = BASE_IMAGE_URL + images.image_url;
    if (images.user) {
      images.user = fixUser(images.user);
    }
    return images;
  }
};

const uploadImage = async (req, res) => {
  try {
    // xử lý hình tải lên
    let { buffer, originalname } = req.files[0];
    let timestamp = Date.now();
    let image_url = `${timestamp}-${originalname}.webp`;
    await sharp(buffer)
      .webp({ quality: 20 })
      .toFile(process.cwd() + "/public/img/" + image_url);
    // thông tin hình tải lên
    let { image_name, image_desc, user_id } = req.body;
    let image = {
      image_name,
      image_url,
      image_desc,
      user_id: +user_id,
    };

    let data = await prisma.image.create({ data: image });
    successCode(res, data, "Tải hình lên thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getImages = async (req, res) => {
  try {
    let images = await prisma.image.findMany({
      orderBy: { image_id: "desc" },
    });
    let data = fixImageURL(images);

    successCode(res, data, "Lấy danh sách hình thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getImagesByName = async (req, res) => {
  try {
    let { image_name } = req.query;

    let images = await prisma.image.findMany({
      where: {
        image_name: {
          contains: image_name,
        },
      },
      orderBy: {
        image_id: "desc",
      },
    });
    let data = fixImageURL(images);

    successCode(res, data, "Lấy danh sách hình theo tên thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getImageDetail = async (req, res) => {
  try {
    let { image_id } = req.params;

    let image = await prisma.image.findFirst({
      where: {
        image_id: +image_id,
      },
      include: {
        user: true,
      },
    });
    if (image) {
      let data = fixImageURL(image);
      successCode(res, data, "Lấy thông tin ảnh và người tạo ảnh thành công");
    } else {
      failCode(res, null, "Không tìm thấy thông tin ảnh và người tạo ảnh");
    }
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const updateImage = async (req, res) => {
  try {
    let { image_id } = req.params;
    let { image_name, image_desc } = req.body;

    let checkImage = await prisma.image.findFirst({
      where: {
        image_id: +image_id,
      },
    });

    if (checkImage) {
      let image = { image_name, image_desc };

      let updateImage = await prisma.image.update({
        where: {
          image_id: +image_id,
        },
        data: image,
      });

      let data = fixImageURL(updateImage);
      successCode(res, data, "Cập nhật thông tin ảnh thành công");
    } else {
      failCode(res, null, "Không tìm thấy thông tin ảnh");
    }
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getComments = async (req, res) => {
  try {
    let { image_id } = req.params;

    let comments = await prisma.comment.findMany({
      where: {
        image_id: +image_id,
      },
      include: {
        user: true,
      },
      orderBy: {
        comment_id: "desc",
      },
    });
    let data = comments.map((comment) => {
      return { ...comment, user: fixUser(comment.user) };
    });

    successCode(res, data, "Lấy thông tin bình luận thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const saveComment = async (req, res) => {
  try {
    let { content, user_id, image_id } = req.body;

    let newCm = {
      content,
      comment_date: new Date(),
      user_id: +user_id,
      image_id: +image_id,
    };
    let comment = await prisma.comment.create({
      data: newCm,
      include: {
        user: true,
      },
    });
    let data = { ...comment, user: fixUser(comment.user) };

    successCode(res, data, "Gửi bình luận thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const updateComment = async (req, res) => {
  try {
    let { comment_id } = req.params;
    let { content } = req.body;

    let updateCm = {
      content,
      comment_date: new Date(),
    };
    let comment = await prisma.comment.update({
      data: updateCm,
      where: {
        comment_id: +comment_id,
      },
      include: {
        user: true,
      },
    });
    let data = { ...comment, user: fixUser(comment.user) };

    successCode(res, data, "Sửa bình luận thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const deleteComment = async (req, res) => {
  try {
    let { comment_id } = req.params;

    await prisma.comment.delete({
      where: {
        comment_id: +comment_id,
      },
    });
    successCode(res, "", "Xóa bình luận thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const hasSavedImage = async (req, res) => {
  try {
    let { user_id, image_id } = req.params;

    let checkImage = await prisma.save.findFirst({
      where: {
        user_id: +user_id,
        image_id: +image_id,
      },
    });

    if (checkImage) {
      successCode(res, true, "Hình đã lưu");
    } else {
      successCode(res, false, "Hình chưa lưu");
    }
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const saveImage = async (req, res) => {
  try {
    let { user_id, image_id } = req.params;

    let checkImage = await prisma.save.findFirst({
      where: {
        user_id: +user_id,
        image_id: +image_id,
      },
    });

    if (checkImage) {
      failCode(res, false, "Hình đã lưu");
    } else {
      let image = {
        save_date: new Date(),
        user_id: +user_id,
        image_id: +image_id,
      };
      let data = await prisma.save.create({
        data: image,
      });
      successCode(res, data, "Lưu hình ảnh thành công");
    }
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const unsaveImage = async (req, res) => {
  try {
    let { user_id, image_id } = req.params;

    let checkImage = await prisma.save.findFirst({
      where: {
        user_id: +user_id,
        image_id: +image_id,
      },
    });

    if (checkImage) {
      let data = await prisma.save.delete({
        where: {
          save_id: checkImage.save_id,
        },
      });
      successCode(res, data, "Xóa hình đã lưu thành công");
    } else {
      failCode(res, false, "Hình chưa lưu");
    }
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getSavedImagesByUser = async (req, res) => {
  try {
    let { user_id } = req.params;

    let saves = await prisma.save.findMany({
      where: {
        user_id: +user_id,
      },
      include: {
        image: true,
      },
      orderBy: {
        save_id: "desc",
      },
    });
    let data = saves.map((save) => {
      save.image = fixImageURL(save.image);
      return save;
    });

    successCode(res, data, "Lấy ảnh đã lưu theo người dùng thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getCreatedImagesByUser = async (req, res) => {
  try {
    let { user_id } = req.params;

    let images = await prisma.image.findMany({
      where: {
        user_id: +user_id,
      },
      orderBy: {
        image_id: "desc",
      },
    });
    let data = fixImageURL(images);

    successCode(res, data, "Lấy ảnh đã tạo theo người dùng thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const deleteImage = async (req, res) => {
  try {
    let { image_id } = req.params;

    let image = await prisma.image.findFirst({
      where: {
        image_id: +image_id,
      },
    });
    if (image) {
      let saves = await prisma.save.findMany({
        where: {
          image_id: +image_id,
        },
      });
      if (saves.length > 0) {
        await prisma.save.deleteMany({
          where: {
            image_id: +image_id,
          },
        });
      }
      let comments = await prisma.comment.findMany({
        where: {
          image_id: +image_id,
        },
      });
      if (comments.length > 0) {
        await prisma.comment.deleteMany({
          where: {
            image_id: +image_id,
          },
        });
      }
      let reactions = await prisma.reaction.findMany({
        where: {
          image_id: +image_id,
        },
      });
      if (reactions.length > 0) {
        await prisma.reaction.deleteMany({
          where: {
            image_id: +image_id,
          },
        });
      }
      let path = process.cwd() + "/public/img/" + image.image_url;
      fs.unlink(path, (err) => {
        if (err) throw err;
      });
      await prisma.image.delete({
        where: {
          image_id: +image_id,
        },
      });
      successCode(res, "", "Xóa hình ảnh thành công");
    } else {
      failCode(res, null, "Không tìm thấy hình ảnh");
    }
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getImageReactions = async (req, res) => {
  try {
    let { image_id } = req.params;

    let reactions = await prisma.reaction.findMany({
      where: {
        image_id: +image_id,
      },
      include: {
        user: true,
      },
      orderBy: {
        reaction_id: "desc",
      },
    });

    let data = reactions.map((reaction) => {
      return { ...reaction, user: fixUser(reaction.user) };
    });

    successCode(res, data, "Lấy danh sách thích hình ảnh thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getImageReactionsAmount = async (req, res) => {
  try {
    let { image_id } = req.params;

    let data = await prisma.reaction.count({
      where: {
        image_id: +image_id,
      },
    });

    successCode(res, data, "Lấy danh sách thích hình ảnh thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getThreeMostImageReactions = async (req, res) => {
  try {
    let { image_id } = req.params;

    let data = await prisma.reaction.groupBy({
      by: ["reaction_type"],
      where: {
        image_id: +image_id,
      },
      _count: {
        reaction_type: true,
      },
      orderBy: {
        _count: {
          reaction_type: "desc",
        },
      },
      take: 3,
    });
    successCode(
      res,
      data,
      "Lấy danh sách 3 lượt thích hình ảnh nhiều nhất thành công"
    );
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const createImageReaction = async (req, res) => {
  try {
    let { reaction_type, user_id, image_id } = req.body;

    let checkReaction = await prisma.reaction.findFirst({
      where: {
        image_id: +image_id,
        user_id: +user_id,
      },
    });

    if (checkReaction) {
      failCode(res, null, "Đã thích hình ảnh này");
    } else {
      let reaction = {
        reaction_type,
        user_id: +user_id,
        image_id: +image_id,
      };

      let data = await prisma.reaction.create({
        data: reaction,
      });
      successCode(res, data, "Thích hình ảnh thành công");
    }
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getImageReactionByUser = async (req, res) => {
  try {
    let { user_id, image_id } = req.params;

    let data = await prisma.reaction.findFirst({
      where: {
        image_id: +image_id,
        user_id: +user_id,
      },
    });
    if (data) {
      successCode(res, data, "Lấy thích hình ảnh theo người dùng thành công");
    } else {
      successCode(res, null, "Không tìm thấy thích hình ảnh theo người dùng");
    }
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const updateImageReaction = async (req, res) => {
  try {
    let { reaction_type, user_id, image_id } = req.body;

    let reaction = await prisma.reaction.findFirst({
      where: {
        user_id: +user_id,
        image_id: +image_id,
      },
    });
    if (reaction) {
      reaction.reaction_type = reaction_type;
      let data = await prisma.reaction.update({
        where: {
          reaction_id: +reaction.reaction_id,
        },
        data: reaction,
      });
      successCode(res, data, "Cập nhật thích hình ảnh thành công");
    } else {
      failCode(res, null, "Không tìm thấy thích hình ảnh");
    }
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const deleteImageReaction = async (req, res) => {
  try {
    let { user_id, image_id } = req.params;

    let reaction = await prisma.reaction.findFirst({
      where: {
        user_id: +user_id,
        image_id: +image_id,
      },
    });
    if (reaction) {
      await prisma.reaction.delete({
        where: {
          reaction_id: reaction.reaction_id,
        },
      });
      successCode(res, "", "Xóa thích hình ảnh thành công");
    } else {
      failCode(res, null, "Không tìm thấy thích hình ảnh");
    }
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

export {
  uploadImage,
  getImages,
  getImagesByName,
  getImageDetail,
  updateImage,
  getComments,
  saveComment,
  updateComment,
  deleteComment,
  hasSavedImage,
  saveImage,
  unsaveImage,
  getSavedImagesByUser,
  getCreatedImagesByUser,
  deleteImage,
  getImageReactions,
  getImageReactionsAmount,
  getThreeMostImageReactions,
  createImageReaction,
  getImageReactionByUser,
  updateImageReaction,
  deleteImageReaction,
};
