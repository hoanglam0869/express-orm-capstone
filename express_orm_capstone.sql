CREATE TABLE user (
  user_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  email VARCHAR(255) DEFAULT NULL,
  password VARCHAR(255) DEFAULT NULL,
  full_name VARCHAR(255) DEFAULT NULL,
  age INT DEFAULT NULL,
  avatar VARCHAR(255) DEFAULT NULL
);

CREATE TABLE image (
  image_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  image_name VARCHAR(255) DEFAULT NULL,
  image_url VARCHAR(255) DEFAULT NULL,
  image_desc TEXT DEFAULT NULL,
  
  user_id INT DEFAULT 1,
  FOREIGN KEY (user_id) REFERENCES user(user_id)
);

CREATE TABLE comment (
  comment_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  content VARCHAR(255) DEFAULT NULL,
  comment_date DATETIME,
  
  user_id INT DEFAULT 1,
  FOREIGN KEY (user_id) REFERENCES user(user_id),
  
  image_id INT DEFAULT 1,
  FOREIGN KEY (image_id) REFERENCES image(image_id)
);

INSERT INTO comment (comment_id, content, user_id, image_id) VALUES
(1, 'Khủng long dễ thương quá!', 3, 1),
(2, 'Trái cây ngon quá!', 2, 3),
(3, 'Flash đẹp trai ghê!', 1, 2),
(4, 'Khủng long tên gì thế?', 2, 1),
(5, 'Nhà bạn bán trái cây hả?', 1, 3),
(6, 'Diễn viên đóng Flash tên gì thế ạ?', 3, 2);

CREATE TABLE save (
  save_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  save_date DATETIME,
	
  user_id INT DEFAULT 1,
  FOREIGN KEY (user_id) REFERENCES user(user_id),
  
  image_id INT DEFAULT 1,
  FOREIGN KEY (image_id) REFERENCES image(image_id)
);

CREATE TABLE reaction (
  reaction_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  reaction_type VARCHAR(255) DEFAULT NULL,
	
  user_id INT DEFAULT 1,
  FOREIGN KEY (user_id) REFERENCES user(user_id),
  
  image_id INT DEFAULT 1,
  FOREIGN KEY (image_id) REFERENCES image(image_id)
);

CREATE TABLE follow (
  follow_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	
  follower_id INT DEFAULT 1,
  FOREIGN KEY (follower_id) REFERENCES user(user_id),
  
  following_id INT DEFAULT 1,
  FOREIGN KEY (following_id) REFERENCES user(user_id)
);

CREATE TABLE reply_comment (
  reply_comment_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  content VARCHAR(255) DEFAULT NULL,
  reply_comment_date DATETIME,
  
  user_id INT DEFAULT 1,
  FOREIGN KEY (user_id) REFERENCES user(user_id),
  
  comment_id INT DEFAULT 1,
  FOREIGN KEY (comment_id) REFERENCES comment(comment_id),

  reply_user_id INT DEFAULT 1,
  FOREIGN KEY (reply_user_id) REFERENCES user(user_id)
);