/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE `binh_luan` (
  `comment_id` int NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `comment_date` datetime DEFAULT NULL,
  `user_id` int DEFAULT '1',
  `image_id` int DEFAULT '1',
  PRIMARY KEY (`comment_id`),
  KEY `user_id` (`user_id`),
  KEY `image_id` (`image_id`),
  CONSTRAINT `binh_luan_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `binh_luan_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `image` (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `image` (
  `image_id` int NOT NULL AUTO_INCREMENT,
  `image_name` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `image_desc` varchar(255) DEFAULT NULL,
  `user_id` int DEFAULT '1',
  PRIMARY KEY (`image_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `image_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `luu_anh` (
  `save_id` int NOT NULL AUTO_INCREMENT,
  `save_date` datetime DEFAULT NULL,
  `user_id` int DEFAULT '1',
  `image_id` int DEFAULT '1',
  PRIMARY KEY (`save_id`),
  KEY `user_id` (`user_id`),
  KEY `image_id` (`image_id`),
  CONSTRAINT `luu_anh_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `luu_anh_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `image` (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `age` int DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `binh_luan` (`comment_id`, `content`, `comment_date`, `user_id`, `image_id`) VALUES
(1, 'Trái cây tươi đây =))', '2023-09-26 07:54:41', 3, 1);
INSERT INTO `binh_luan` (`comment_id`, `content`, `comment_date`, `user_id`, `image_id`) VALUES
(2, 'Phòng khách sang trọng ghê :v', '2023-09-26 07:55:04', 3, 2);
INSERT INTO `binh_luan` (`comment_id`, `content`, `comment_date`, `user_id`, `image_id`) VALUES
(3, 'Flash ngầu quá o.o', '2023-09-26 07:55:55', 3, 3);
INSERT INTO `binh_luan` (`comment_id`, `content`, `comment_date`, `user_id`, `image_id`) VALUES
(4, 'Giáng sinh ấm áp nha mọi người <3', '2023-09-26 07:56:49', 3, 4);

INSERT INTO `image` (`image_id`, `image_name`, `image_url`, `image_desc`, `user_id`) VALUES
(1, 'Trái cây', '1695639268142_fruit.jpg', '', 1);
INSERT INTO `image` (`image_id`, `image_name`, `image_url`, `image_desc`, `user_id`) VALUES
(2, 'Phòng khách', '1695639569197_cambg_7.jpg', '', 1);
INSERT INTO `image` (`image_id`, `image_name`, `image_url`, `image_desc`, `user_id`) VALUES
(3, 'The Flash', '1695639638697_flash.jpeg', '', 2);
INSERT INTO `image` (`image_id`, `image_name`, `image_url`, `image_desc`, `user_id`) VALUES
(4, 'Giáng sinh', '1695639669557_cambg_3.jpg', '', 2),
(5, 'Hoa hướng dương', '1695639783168_sunflower.jpg', '', 3),
(6, 'Bức tường', '1695639821650_cambg_4.jpg', '', 3);

INSERT INTO `luu_anh` (`save_id`, `save_date`, `user_id`, `image_id`) VALUES
(1, '2023-09-25 11:05:02', 3, 2);
INSERT INTO `luu_anh` (`save_id`, `save_date`, `user_id`, `image_id`) VALUES
(2, '2023-09-25 11:05:07', 3, 4);
INSERT INTO `luu_anh` (`save_id`, `save_date`, `user_id`, `image_id`) VALUES
(3, '2023-09-25 11:05:10', 3, 6);
INSERT INTO `luu_anh` (`save_id`, `save_date`, `user_id`, `image_id`) VALUES
(5, '2023-09-26 07:57:44', 1, 1),
(6, '2023-09-26 07:57:49', 1, 3),
(7, '2023-09-26 07:57:51', 1, 5);

INSERT INTO `user` (`user_id`, `email`, `password`, `full_name`, `age`, `avatar`) VALUES
(1, 'khanhnhi@gmail.com', '$2b$10$mb.CIPnRlemdpCbSe80G3OCJhTpL79bjCzdm5u4a6RDojBew2bbqG', 'Khánh Nhi', 21, '1695639283331_khanh_nhi.png');
INSERT INTO `user` (`user_id`, `email`, `password`, `full_name`, `age`, `avatar`) VALUES
(2, 'ducbinh@gmail.com', '$2b$10$StkyB6VtMjHfuGYDBrz2nO5T9JhLLotg1rilL6Yn3fBeLBMatWF0.', 'Đức Bình', 23, '1695639689315_duc-binh.png');
INSERT INTO `user` (`user_id`, `email`, `password`, `full_name`, `age`, `avatar`) VALUES
(3, 'hoanglam@gmail.com', '$2b$10$tK/AkFQYbs6RX0puDe89DuldjysZevf5At3pnyFFyelG60cW9njo6', 'Hoàng Lâm', 30, '1695639872640_minh-an.png');


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;