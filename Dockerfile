FROM node:16

WORKDIR /usr/src/express_orm_capstone

COPY package*.json .

RUN yarn install --legacy-peer-deps

COPY prisma ./prisma

RUN yarn prisma generate

COPY . .

# private port sẽ chạy
EXPOSE 8081

# node index.js => khởi chạy server
CMD ["node", "src/index.js"]

# docker build . -t img-express-orm-capstone

# docker run -d -e DATABASE_URL=mysql://freedb_hoanglam0869:aM%23P%25fbuB8%405bNt@sql.freedb.tech:3306/freedb_express_orm_capstone?schema=public -p 4000:8081 --name cons-express-orm-capstone img-express-orm-capstone